# memo forge

## Paramétrages du clone de répertoire avec GIT

- dans un terminal:
git clone https://forge.apps.education.fr/ALIAS/CHEMIN_DU_PROJET
git config --global user.name ALIAS
git config --global user.email PRENOM.NOM@ac-lyon.fr


## Créer clé SSH

- dans un terminal:
ssh-keygen -t ed25519 -C PRENOM.NOM@ac-lyon.fr
  → création d'un mot de passe "keyphrase" à cet instant

- ouvrir avec un éditeur de texte le fichier du PC:
/home/USER/.ssh/id_ed25519.pub

- copier la clé publique à l'intérieur de ce fichier

- dans la forge en ligne:
	- aller dans les paramètres du compte
	- aller dans "Clé SSH"
	- "Ajouter une nouvelle clé"
	- coller ici la clé publique récupérée


## accès depuis VSCodium en SSH

- dans VSCodium, installer les extensions
   - GitLab Workflow
   - Python (ms-python)

- dans un terminal:
ssh -T git@forge.apps.education.fr

- puis dans un terminal ouvert dans le dossier du projet:
git remote set-url origin git@forge.apps.education.fr:ALIAS/NOM_DU_PROJET.git

- lancer VSCodium
- ouvrir le dossier local du projet
- au moment de la première mise à jour (pull ou push), entrer la "keyphrase" créée précédemment